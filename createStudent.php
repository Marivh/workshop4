<?php
  include('functions.php');

  if(isset($_POST['full_name']) && isset($_POST['email']) && isset($_POST['idcareer']) ) {
    $saved = saveStudent($_POST);

    if($saved) {
      header('Location: /prograWeb/crud/?status=success');
    } else {
      header('Location: /prograWeb/crud/?status=error');
    }
  } else {
    header('Location: /prograWeb/crud/?status=error');
  }
