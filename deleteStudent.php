<?php
include('functions.php');

$id = $_GET['id'];
if($id) {
  $student = getStudent($id);
  if($student) {
    $deleted = deleteStudent($id);
    if($deleted) {
      header('Location: /prograWeb/crud/?status=success');
    } else {
      header('Location: /prograWeb/crud/?status=error');
    }
  } else {
    header('Location: /prograWeb/crud/?status=error');
  }
} else {
  header('Location: /prograWeb/crud/index.php');
}